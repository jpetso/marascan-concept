import QtQuick 2.6
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

Kirigami.ScrollablePage {
    id: root

    readonly property GlobalScanAction globalMainAction: GlobalScanAction {
        owningPage: root
    }
    actions.main: globalMainAction.canShowTitleDelegate ? null : globalMainAction
    Binding {
        target: root
        property: "titleDelegate"
        when: globalMainAction.canShowTitleDelegate
        value: globalMainAction.titleDelegate
    }

    actions.left: Kirigami.Action {
        id: cancelPresetChanges
        icon.name: "dialog-cancel"
        text: i18nc("@action:button", "Cancel")
        shortcut: StandardKey.Cancel
        onTriggered: applicationWindow().popSidebar(root)
    }

    actions.right: Kirigami.Action {
        id: confirmPresetChanges
        icon.name: "dialog-ok"
        text: i18nc("@action:button", "Confirm")
        onTriggered: applicationWindow().popSidebar(root)
    }

    contextualActions: [
        Kirigami.Action {
            text: i18nc("@action:button", "Clone Preset")
            iconName: "edit-duplicate"
            onTriggered: showPassiveNotification("Preset cloned! ...NOT")
            displayHint: Kirigami.Action.DisplayHint.AlwaysHide
        },
        Kirigami.Action {
            text: i18nc("@action:button", "Delete Preset")
            iconName: "edit-delete"
            onTriggered: showPassiveNotification("Preset deleted! ...NOT")
            displayHint: Kirigami.Action.DisplayHint.AlwaysHide
        }
    ]

    Kirigami.FormLayout {
        Controls.TextField {
            id: nameField
            Kirigami.FormData.label: i18nc("@label:textbox", "Preset:")
            placeholderText: i18n("Name")
            text: mode === "add" ? "" : name
            onAccepted: descriptionField.forceActiveFocus()
        }
        Controls.TextField {
            id: descriptionField
            Kirigami.FormData.label: i18nc("@label:textbox", "Option 1:")
            placeholderText: i18n("Description")
            text: mode === "add" ? "" : description
            onAccepted: dateField.forceActiveFocus()
        }
        Controls.TextField {
            id: dateField
            Kirigami.FormData.label: i18nc("@label:textbox", "Option 2:")
            inputMask: "0000-00-00"
            placeholderText: i18n("YYYY-MM-DD")
            text: mode === "add" ? "" : kdate
        }
    }
}
