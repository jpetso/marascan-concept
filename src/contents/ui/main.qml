import QtQuick 2.15
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

// Base element, provides basic features needed for all kirigami applications
ApplicationWindowWithSidebarStack {
    id: root

    property int numPagesPlaceholder: 0
    readonly property bool containsPagesOrMetadata: numPagesPlaceholder > 0

    function discardDocument() {
        root.numPagesPlaceholder = 0; // TODO
    }
    function saveDocument() {
        showPassiveNotification("Save ...tbd"); // TODO
    }
    function scan() {
        root.numPagesPlaceholder = root.numPagesPlaceholder + 1; // TODO
    }

    property QtObject selectedDevice: null
    property Kirigami.Page deviceSelectionPage: (
        pageStack.items.length > 0 ? pageStack.get(0) : null
    )
    property Kirigami.Page scanOptionsPage: null

    onSelectedDeviceChanged: {
        if (selectedDevice) {
            scanOptionsPage = pushSidebar(scanOptionsComponent, scanOptionsPage);
        }
        else {
            if (scanOptionsPage) {
                popSidebar(scanOptionsPage);
            }
            scanOptionsPage = null;
        }
    }

    title: i18nc("@title:window", "Marascan Concept")

    initialSidebarPage: Qt.resolvedUrl("ImageSourceSelection.qml")

    mainPage: Component {
        PageGrid {}
    }
    mainPageActive: containsPagesOrMetadata

    Component {
        id: scanOptionsComponent

        ScanOptions {
            selectedDevice: root.selectedDevice
        }
    }

    globalDrawer: Kirigami.GlobalDrawer {
        isMenu: true
        actions: [
            Kirigami.Action {
                id: discardDocumentAction
                text: i18nc("@action:button Throw away existing pages/metadata and start from scratch", "Discard")
                icon.name: "document-replace"
                shortcut: StandardKey.Close
                onTriggered: discardDocument()
                enabled: root.containsPagesOrMetadata
            },
            Kirigami.Action {
                id: importDocumentAction
                icon.name: "document-import"
                text: i18nc("@action:button", "Import")
                onTriggered: showPassiveNotification("Import ...tbd") // TODO
            },
            Kirigami.Action {
                id: insertBlankPageAction
                icon.name: "page-simple"
                text: i18nc("@action:button", "Insert Blank Page")
                onTriggered: showPassiveNotification("Insert Blank Page ...tbd") // TODO
            },
            // the following actions are meant to show up in an icon-only row to save menu space ->
            Kirigami.Action {
                id: saveDocumentAction
                text: i18nc("@action:button", "Save")
                icon.name: "document-save"
                shortcut: StandardKey.Save
                onTriggered: saveDocument()
                enabled: root.containsPagesOrMetadata
            },
            Kirigami.Action {
                id: shareDocumentAction
                text: i18nc("@action:button", "Share")
                icon.name: "document-share"
                onTriggered: showPassiveNotification("Share ...tbd") // TODO
            },
            Kirigami.Action {
                id: printDocumentAction
                text: i18nc("@action:button", "Print")
                icon.name: "document-print"
                shortcut: StandardKey.Print
                onTriggered: showPassiveNotification("Print ...tbd") // TODO
            },
            // <- end of planned icon-row actions
            Kirigami.Action {
                id: settingsAction
                icon.name: "settings-configure"
                text: i18nc("@action:button", "Settings")
                shortcut: StandardKey.Preferences
                onTriggered: showPassiveNotification("Settings ...tbd") // TODO
            },
            Kirigami.Action {
                id: helpAction
                icon.name: "help-contents"
                text: i18nc("@action:button", "Help")
                shortcut: StandardKey.HelpContents
                onTriggered: showPassiveNotification("Help ...tbd") // TODO
            },
            Kirigami.Action {
                id: aboutAction
                icon.name: "help-about"
                text: i18nc("@action:button", "About %1", Qt.application.name)
                onTriggered: showPassiveNotification("About Marascan ...tbd") // TODO
            }
        ]
    }
}
