import QtQml 2.15
import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

Kirigami.ScrollablePage {
    id: root

    title: showTitle ? i18nc("@title:page", "Scan Options") : ""
    readonly property bool showTitle: {
        globalToolBarStyle != Kirigami.ApplicationHeaderStyle.ToolBar
    }

    required property QtObject selectedDevice

    readonly property GlobalMainAction globalMainAction: GlobalScanAction {
        owningPage: root
    }
    actions.main: globalMainAction.canShowTitleDelegate ? null : globalMainAction
    Binding {
        target: root
        property: "titleDelegate"
        when: globalMainAction.canShowTitleDelegate
        value: globalMainAction.titleDelegate
    }

    actions.right: Kirigami.Action {
        id: importDocument
        icon.name: "document-import"
        text: i18nc("@action:button", "Import")
        //displayHint: Kirigami.Action.DisplayHint.IconOnly
        onTriggered: showPassiveNotification("Import ...tbd")
    }

    Component {
        id: editQualityPresetComponent

        EditPresetBase { // TODO: use a derived type instead, this is just an example
        }
    }

    Kirigami.FormLayout {
        Controls.ToolButton {
            Layout.fillWidth: true
            Kirigami.FormData.label: i18nc(
                "@label:button Select image acquisition device (scanner, camera, ...)",
                "Device:")

            text: (
                selectedDevice !== null ? selectedDevice.displayName : i18nc("@text:button Image acquisition device", "No device selected")
            )
            onClicked: {
                applicationWindow().focusSidebarPage(
                        applicationWindow().deviceSelectionPage)
            }
        }

        ColumnLayout {
            Layout.fillWidth: true
            Kirigami.FormData.label: i18nc("@label:combobox", "Quality Preset:")

            RowLayout {
                Layout.fillWidth: true
                Controls.ComboBox {
                    Layout.fillWidth: true
                    id: qualityPresetComboBox

                    model: ListModel {
                        id: qualityPresetModel
                        ListElement {
                            text: "Text (color)"
                            description: "Color\n300 dpi"
                        }
                        ListElement {
                            text: "Text (grayscale)"
                            description: "Grayscale\n300 dpi"
                        }
                        ListElement {
                            text: "Image"
                            description: "Color\n600 dpi"
                        }
                    }

                    textRole: "text"
                    valueRole: "description"
                }
                Controls.Button {
                    icon.name: "edit-entry"
                    onClicked: applicationWindow().pushSidebar(editQualityPresetComponent)
                }
            }

            RowLayout {
                Layout.fillWidth: true
                Rectangle {
                    Layout.leftMargin: Kirigami.Units.smallSpacing
                    color: Kirigami.ColorUtils.linearInterpolation(
                        Kirigami.Theme.backgroundColor,
                        Kirigami.Theme.disabledTextColor,
                        0.5
                    )
                    width: Kirigami.Units.smallSpacing
                    Layout.fillHeight: true
                }
                Controls.Label {
                    id: qualityPresetSummary
                    Layout.leftMargin: Kirigami.Units.smallSpacing
                    Layout.fillWidth: true
                    text: qualityPresetComboBox.currentValue
                    color: Kirigami.Theme.disabledTextColor
                }
            }
        }
    }
}
