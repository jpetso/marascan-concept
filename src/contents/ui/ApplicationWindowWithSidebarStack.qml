import QtQuick 2.15
import org.kde.kirigami 2.13 as Kirigami

// Base element, provides basic features needed for all kirigami applications
Kirigami.ApplicationWindow {
    required property variant initialSidebarPage
    required property Component mainPage
    property bool mainPageActive: true

    // Initial page to be loaded on app load
    pageStack.initialPage: initialSidebarPage

    // The Page object created by the mainPage component, managed by this QML type
    property Kirigami.Page mainPageInstance: null
    property bool mainPageVisible: mainPageInstance && (mainPageInstance.isCurrentPage || pageStack.wideMode)

    // The following condition is used by PageRow.wideMode, but there it's
    // coupled with depth >= 2 which isn't what we're looking for here.
    // We need to know whether we'd be in wide mode also if depth == 1.
    readonly property bool wideModePossible: pageStack.width >= pageStack.defaultColumnWidth * 2

    onWideModePossibleChanged: updateMainPage(true)
    onMainPageActiveChanged: updateMainPage()

    function updateMainPage(preserveCurrentIndex = false) {
        const shouldExist = wideModePossible || mainPageActive;

        if (!mainPageInstance && shouldExist) {
            const preservedIndex = pageStack.currentIndex;
            mainPageInstance = mainPage.createObject();
            pageStack.push(mainPageInstance);
            if (preserveCurrentIndex) {
                pageStack.currentIndex = preservedIndex;
            }
        }
        else if (mainPageInstance && !shouldExist) {
            pageStack.pop();
            mainPageInstance = null;
        }
    }

    property int $privateIndexToPop: -1
    Connections {
        target: pageStack.columnView
        function onMovingChanged() {
            if (!pageStack.moving && $privateIndexToPop >= 0) {
                const sidebarTopIndex = pageStack.items.length - (mainPageInstance ? 2 : 1);
                for (var i = sidebarTopIndex; i >= $privateIndexToPop; --i) {
                    pageStack.removePage(i);
                }
                $privateIndexToPop = -1;
            }
        }
    }
    function pushSidebar(pageComponent, replacedPage) {
        const pages = mainPageInstance ? [pageComponent, mainPage] : [pageComponent];
        const sidebarTopIndex = pageStack.items.length - pages.length;
        const indexToInsert = indexOfPageOr(replacedPage, sidebarTopIndex + 1);

        pageStack.insertPage(indexToInsert, pages);
        mainPageInstance = mainPageInstance ? pageStack.get(pageStack.items.length - 1) : null;
        pageStack.currentIndex = pageStack.items.length - (mainPageInstance ? 2 : 1);

        return pageStack.get(pageStack.currentIndex);
    }
    function popSidebar(page) {
        const sidebarTopIndex = pageStack.items.length - (mainPageInstance ? 2 : 1);
        const indexToRemove = indexOfPageOr(page, sidebarTopIndex);

        $privateIndexToPop = indexToRemove;
        pageStack.currentIndex = indexToRemove - 1;
    }
    function focusSidebarPage(page) {
        pageStack.currentIndex = indexOfPageOr(page, pageStack.currentIndex);
    }
    function indexOfPageOr(page, notFoundResult) {
        if (page) {
            for (var i = 0; i < pageStack.items.length; ++i) {
                if (pageStack.items[i] == page) { return i; }
            }
        }
        return notFoundResult;
    }

    Component.onCompleted: updateMainPage()
}
