import QtQuick 2.6
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

Kirigami.ScrollablePage {
    id: root

    Kirigami.ColumnView.pinned: true

    property bool arePagesSelected: false

    readonly property GlobalScanAction globalMainAction: GlobalScanAction {
        owningPage: root
    }
    readonly property Kirigami.Action discardDocumentAction: Kirigami.Action {
        text: i18nc("@action:button Throw away existing pages/metadata and start from scratch", "Discard")
        icon.name: "document-replace"
        // shortcut: StandardKey.Close // main.qml already handles the shortcut
        enabled: applicationWindow().containsPagesOrMetadata
        onTriggered: applicationWindow().discardDocument()
    }
    readonly property Kirigami.Action deletePagesAction: Kirigami.Action {
        icon.name: "edit-delete"
        text: i18nc("@action:button", "Delete")
        onTriggered: showPassiveNotification("Delete Page(s) ...tbd")
    }

    actions.main: globalMainAction.canShowTitleDelegate ? null : globalMainAction
    Binding {
        target: root
        property: "titleDelegate"
        when: globalMainAction.canShowTitleDelegate
        value: globalMainAction.titleDelegate
    }

    actions.left: arePagesSelected ? deletePagesAction : discardDocumentAction

    actions.right: Kirigami.Action {
        id: saveDocumentAction
        icon.name: "document-save"
        text: i18nc("@action:button", "Save")
        // shortcut: StandardKey.Save // main.qml already handles the shortcut
        onTriggered: showPassiveNotification("Save ...tbd")
        enabled: applicationWindow().containsPagesOrMetadata
    }

    GridView {
        id: pageGrid
        model: applicationWindow().numPagesPlaceholder
        implicitWidth: Kirigami.Units.gridUnit * 30
        cellWidth: Math.max(Kirigami.Units.gridUnit * 10, Math.floor(width / Math.floor(width / (Kirigami.Units.gridUnit * 10))))
        cellHeight: cellWidth
        delegate: Item {
            width: pageGrid.cellWidth
            height: pageGrid.cellHeight
            Rectangle {
                color: Kirigami.Theme.highlightColor
                radius: Kirigami.Units.gridUnit
                anchors.centerIn: parent
                width: Math.round(pageGrid.cellWidth * 0.8)
                height: width
            }
        }
    }

}
