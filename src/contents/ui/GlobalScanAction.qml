import QtQml 2.15
import org.kde.kirigami 2.13 as Kirigami

GlobalMainAction {
    icon.name: "document-scan"
    text: i18nc("@action:button", "Scan")
    displayHint: Kirigami.Action.DisplayHint.KeepVisible
    onTriggered: applicationWindow().scan()
    enabled: applicationWindow().selectedDevice !== null
}
