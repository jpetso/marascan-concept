import QtQml 2.15
import QtQuick 2.15
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

Controls.Button {
    id: root

    required property QtObject device

    highlighted: device == applicationWindow().selectedDevice

    implicitHeight: implicitContentHeight
    implicitWidth: ListView.view ? ListView.view.width : implicitContentWidth

    contentItem: ColumnLayout {
        spacing: 0

        Text {
            text: device.displayName
            elide: Text.ElideRight
            padding: Kirigami.Units.largeSpacing
            color: ((root.highlighted || root.activeFocus)
                    ? Kirigami.Theme.highlightedTextColor
                    : Kirigami.Theme.textColor)
            bottomPadding: 0
        }
        Text {
            topPadding: 0
            text: device.device
            elide: Text.ElideLeft
            color: Kirigami.Theme.disabledTextColor
            padding: Kirigami.Units.largeSpacing
        }
    }
    onClicked: {
        applicationWindow().selectedDevice = device;
        applicationWindow().focusSidebarPage(applicationWindow().scanOptionsPage);
    }
}
