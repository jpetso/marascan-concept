import QtQml 2.15
import QtQuick 2.15
import QtQml.Models 2.1 as Models
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

Kirigami.Page {
    id: root

    title: showTitle ? pageHeading.text : ""
    readonly property bool showTitle: {
        globalToolBarStyle != Kirigami.ApplicationHeaderStyle.ToolBar
    }

    actions.main: Kirigami.Action {
        text: i18nc("@label:button Refresh image source device list", "Refresh")
        icon.name: "view-refresh"
        onTriggered: root.refreshDevices()
    }

    actions.right: Kirigami.Action {
        id: importDocument
        icon.name: "document-import"
        text: i18nc("@action:button", "Import")
        //displayHint: Kirigami.Action.DisplayHint.IconOnly
        onTriggered: showPassiveNotification("Import ...tbd")
    }

    property Models.ObjectModel devicesModel: mockEmptyModel

    property Models.ObjectModel mockEmptyModel: Models.ObjectModel {}
    property Models.ObjectModel mockDevicesModel: Models.ObjectModel {
        DeviceButton {
            device: QtObject {
                readonly property string displayName: "NoName Integrated Camera: Integrated C"
                readonly property string device: "/dev/fooscan"
            }
        }
        DeviceButton {
            device: QtObject {
                readonly property string displayName: "eSCL HP LaserJet Pro M428f-M429f [A4AF31]"
                readonly property string device: "/dev/barscan"
            }
        }
    }

    function refreshDevices() {
        applicationWindow().selectedDevice = null;
        root.devicesModel = mockEmptyModel;
        mockRefreshDevicesTimer.start();
    }

    Timer {
        id: mockRefreshDevicesTimer
        interval: 1500
        running: true
        onTriggered: root.devicesModel = root.mockDevicesModel
    }

    ColumnLayout {
        anchors.fill: parent

        Kirigami.Heading {
            id: pageHeading
            text: i18nc("@title:page", "Select Device")
            type: Kirigami.Heading.Type.Secondary
            visible: !showTitle
        }
        Controls.BusyIndicator {
            visible: mockRefreshDevicesTimer.running == true
            bottomPadding: Kirigami.Units.smallSpacing
        }
        Controls.Label {
            topPadding: 0
            visible: mockRefreshDevicesTimer.running == true || root.devicesModel.length == 0
            text: (mockRefreshDevicesTimer.running == true
                ? i18nc("@label:info", "Scanning for available devices.")
                : i18nc("@label:info", "No devices found."))
            color: Kirigami.Theme.disabledTextColor
        }
        ListView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            boundsBehavior: Flickable.StopAtBounds
            model: root.devicesModel
        }
    }
}
