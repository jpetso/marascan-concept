import QtQml 2.15
import QtQuick 2.6
import QtQuick.Controls 2.0 as Controls
import org.kde.kirigami 2.13 as Kirigami

// Assign to actions.main as well as titleDelegate, will be visible in one of both:
// - primary action (main) when the toolbar isn't visible
// - prominent toolbar button (instead of title text) if toolbar is available
// This allows a good layout for desktop while also being optimal for mobile.
Kirigami.Action {
    id: root

    required property Kirigami.Page owningPage

    function firstPageWithGlobalMainAction(items) {
        for (var i = 0; i < items.length; ++i) {
            if (items[i].globalMainAction) { return items[i]; }
        }
        return null;
    }

    property bool originallyVisible: false
    readonly property bool isFirstSuitablePage: {
        applicationWindow().mainPageVisible
        ? owningPage == applicationWindow().mainPageInstance
        : owningPage == firstPageWithGlobalMainAction(applicationWindow().pageStack.visibleItems)
    }
    readonly property bool visibleOnThisPage: isFirstSuitablePage && originallyVisible
    readonly property bool canShowTitleDelegate: owningPage.globalToolBarStyle == Kirigami.ApplicationHeaderStyle.ToolBar

    Component.onCompleted: {
        originallyVisible = visible
        visible = Qt.binding(function() { return visibleOnThisPage; })
    }

    Component {
        id: toolButtonComponent

        Controls.ToolButton {
            text: root.text
            icon: root.icon
            enabled: root.enabled
            visible: root.visible && root.canShowTitleDelegate
            onClicked: root.onTriggered()
        }
    }
    readonly property Component titleDelegate: toolButtonComponent
}
