Comparative analysis
====================

Let's have a look at how some other apps get (part of) the job done - what features they support, and how.


That app for Mac I once used
----------------------------

Not sure about its name, I think it was an HP app, but simpler than HP Easy Scan. I'm starting with this because it was really just a simple dialog with just a handful of buttons, close to this:

* Scan Document
* Scan Photo
* Settings

Settings would open another dialog with the usual scanner settings (gray/color, DPI, paper format, etc.) which can be set independently for Document and Photo mode. While scanning, another minimal dialog would just display a "Scanning page N" status text, and cancel button. Once a page is scanned, the user can select to scan another page, cancel or save the scan, which opens a Save File dialog and Apple Preview (like Okular). At this point, the flow returns to the original simple dialog.

Takeaways:

* A basic scan workflow doesn't need a lot of options, even a preview isn't strictly necessary.
* Presets for documents vs. photos avoid the need for manually tweaking individual settings on a regular basis.
    * It may be useful to allow for more than two presets - e.g. scanning sheet music requires a higher DPI than scanning text documents, but doesn't have the color and resolution requirements of photo scanning.
* Without previews, one has to keep track of which pages were already scanned vs. still missing. The page number helps with this, but is not as useful as seeing what's available already.


GNOME Document Scanner a.k.a. simple-scan
-----------------------------------------

The default window for Document Scanner is larger than the aforementioned HP scanning dialog app, but smaller than the default Skanlite window. No menu, CSD to save space by integrating its few controls into the titlebar. Document Scanner uses a single window for everything except its Preference dialog, the window is roughly structured into three parts:

* Top ("basic use") toolbar:
    * "Scan" button
        * Note that this is not an icon button, it's spelled out to draw attention
    * Page Settings dropdown (icon only, displaying "Text" or "Image" icon based on current setting)
        * Multi-page modes: Single Page, All Pages from Feeder, Multiple Pages from Flatbed
        * Text / Image
        * Preferences button: opens the Preferences dialog
    * Save button
        * Format support: PDF (default), JPEG, PNG, WebP
    * Hamburger button, menu entries:
        * Email
        * Print
        * Reorder Pages (opens dialog with Combine sides, Combine sides (reverse), Reverse, Keep unchanged)
        * Preferences: another entry point to the Preferences dialog
        * Keyboard Shortcuts, Help, About Document Scanner
* "Document" area: the main workspace
    * At startup, it's covered by "Ready to scan" (plus icon) as well as an image source dropdown (plus refresh button) to allow switching between scanners
        * Shows camera as image source, but can't scan - clearly not designed for laptop cameras
    * After scanning pages, it shows the contents of the scanned page(s)
        * If scanning multiple pages, they get stacked next to each other horizontally
        * Page resizes according to window height, width <= fit to view
* Bottom ("edit") toolbar:
    * "New Document" button: reverts to "Ready to scan" page after confirmation dialog
        * Note that this is not an icon button, it's spelled out to draw attention
    * "Rotate Page" (clockwise / counter-clockwise) icon buttons
    * "Crop" icon button
        * Displays crop rectangle, but no "confirm" action - keeps crop rectangle per page, will move it on any drag & drop (meh)
    * "Delete the selected page" button

The Preferences dialog allows the following settings:

* Scanning
    * Scan Sides: Front, Back, Both
    * Page Size: Automatic, A4, Letter, etc.
    * Delay in Seconds (Interval to scan multiple pages): 0, 3, 6, 10, 15
* Quality
    * Text resolution (for the Text mode): dropdown with dpi values
    * Image Resolution (for the Image mode): dropdown with dpi values
    * Brightness (independent of mode?): a slider
    * Contrast (independent of mode?): a slider

Takeaways:

* Document Scanner offers most relevant options in a simple interface
* Prominent "Scan" button is always visible (sometimes disabled), easy to use
* Page selection (for e.g. "Delete page" button) isn't obvious, no selection color
* Document area could have been used for initial Page Settings (as in dropdown)
* Horizontal page alignment displays nicely for multi-page documents
    * But is it also best for vertical layouts?
    * Automatic resizing means no zoom options for scanned pages
* No Preview action exists in this app - Scan is final scan, bad pages can be deleted
* DPI is the only preset-specific option, page format etc. is shared between modes
    * If DPI is the only difference, why not show DPI in Page Settings dropdown directly? I figure they want to make it easy for very basic users who know the difference between Text and Image, but not 300 dpi and 600 dpi


PDF Arranger
------------

In a nutshell, PDF Arranger is for PDF post-processing what simple-scan is for PDF scanning. Simple & straightforward for the main use cases while actually covering a good set of editing functionality in its hamburger menu. I think it's worth pointing out that there are several prominent UI elements common to both apps:

* Main view with CSD actions and a main view displaying a sequence of pages
    * Zooming is different here: no auto-resize, instead has two zoom levels:
        * "Preview" size, allowing for several side-by-side pages
        * "Page view" size, about 2-3 times the height/width of "Preview"
        * Toggle between both modes via double-click
        * Zoom in/out available as CSD action icons or "+"/"-" shortcuts (also via menu)
    * Scrolling is vertical here, pages automatically reflow based on size
* "Save" icon button
* "Rotate Page" (clockwise / counter-clockwise) icon buttons
    * simple-scan puts those in a bottom toolbar, PDF Arranger has them in the CSD titlebar because there's a status bar at the bottom
* Hamburger menu

There are also some additional features not found in simple-scan:

* "Import" icon button, or "Open a file and append it to the current document" as the hover text calls it (menu calls it "Import")
* "Save As" icon button in addition to "Save", because both modifying PDFs and creating modified sources are common use cases
* "Number of Pages" text field, displaying e.g. "12"
* Ability to drag and drop pages in the main view (the central functionality)
* Status bar with "Selected pages: `$N[-$M]`" text
    * Imho, this could have been combined with "Number of Pages" either in the CSD titlebar (to save space) or in the status bar (to show status in one location)
* In the hamburger menu:
    * "Export" for various pages vs. files save options:
        * Export Selection to a Single File
        * Export Selection to Individual Files
        * Export All Pages to Individual Files
    * Undo/Redo
    * "Edit" submenu:
        * Rotate Left/Right
        * Page Format (scale or crop to percentage of size)
        * Crop White Borders
        * Delete (selected page(s))
        * Duplicate
        * Reverse Order (of selected pages)
        * Split Pages (into columns/rows, specified via % width/height)
        * Cut/Copy
        * Various "Paste" actions (Before, After, Interleave Odd, Interleave Even)
        * Insert Blank Page
    * "Select" submenu:
        * Select All
        * Deselect All
        * Select Odd Pages
        * Select Even Pages
        * All From Same File (interesting - it keeps track of file origins)
        * Same Page Format
        * Invert Selection
    * Zoom In
    * Zoom Out
    * Edit Properties (document properties, e.g. Title, Subject, Keywords, etc.)

Notably missing:

* There's no "crop" action like in simple-scan which even has a prominent icon for it. There is, however a "Crop White Borders" action in the "Edit" submenu.
* "Delete the selected page(s)" doesn't get an icon like in simple-scan, but still has a menu action ("Edit" submenu) and the obvious Delete keyboard shortcut.

Takeaways:

* Many of PDF Arranger's features seem useful even just for a scanning app without PDF modification features. I think the feature set makes a good case for a combined scanning / PDF organization app, there's a lot of natural overlap.


Kooka
-----

An early KDE scanning app, possibly discontinued. Feature-rich but the UI is messy. Not getting into details, just some notable features not found in other scanning apps:

* OCR integration
* "Gallery" view, displaying image files in certain folders
    * This makes it seem a little more like a scanned-page library manager and less like a scan-and-forget utility
* Uses left-side folding widgets (like e.g. in Kate, IDEs) to show/hide the "Scan" settings/action form, as well as Gallery and OCR.
* "Mirror vertically/horizontally" toolbar action
* "Preview", as alternative to "Start Scan"
* Groups scanner settings into three tabs:
    * Basic: gray/color, resolution, "Scan Area" / page format, "Scan source" e.g. "Flatbed"
    * Other
    * Advanced

The window is designed to be large both horizontally and vertically, opening a side widget shifts the page view to the side as opposed to overlapping it temporarily.

Multi-page/PDF does not seem supported, judging by screenshots.

Takeaways:

* Having a sidebar notably reduces the space for actual pages.
* Kooka asks for the scan device at startup in a modal dialog - feels worse than inline selection.
    * The "Always use this device" option is a crutch because it realizes that the dialog is annoying, but shoves it aside instead of improving the device selection UX.
* As a user, how do I determine whether to look for an option in "Other" or "Advanced"? Seems arbitrary. At that point, may as well combine both of these into one.


Skanlite
--------

The current KDE scanning app, afaik a reaction to or or derivative of the overloaded UI of Kooka, throwing out OCR, Gallery and advanced editing functionality for a simpler UI. However, compared to the aforementioned GNOME apps it still feels chunky and poorly designed. The window is roughly structured into three parts:

* Left sidebar: Scanning options, divided into:
    * Basic Options: gray/color, brightness, contrast, white level, invert colors, "Scan Area Size" (page format with mm offsets)
    * Scanner Specific Options: makes widgets out of SANE feature set reports
    * Sidebar can be folded away with a barely noticeable, but compact, arrow in the page view
* Page view - only used for "Preview" scan
    * Shows scrollbars even by default
* Bottom actions (two rows):
    * "Preview" - scans into the main page view
    * "Scan" - scans into a pop-up window page view with "Save"/"Discard" options
    * Mini icon buttons:
        * "Zoom In" / "Zoom Out" / "Zoom to Selection" / "Zoom to Fit"
        * "Clear Selections": Allows crop-like rectangle in the (preview) page view, but instead of cropping it sets the "Scan Area Size" for the subsequent final scan
    * "Help", "About", "Close"
    * "Settings" - opens dialog with persistent settings
        * "Image saving"
            * "Preview before saving": skips pop-up window and skips right to File Save dialog
            * "Save mode": "Open the save dialog for every image / for the first image only"
            * "Save location": default directory when opening Save File dialog
            * "Name & Format": default filename (incl. "####" and image format)
            * "Specify save quality": JPEG quality level etc., if default isn't good
        * "General"
            * "Set preview resolution (DPI)": if not overriding, gets default from scanner (I think?)
            * "Disable automatic selections": by default, Skanlite will try to guess the page size from an initial preview scan instead of selecting a paper size - this option leaves scan size to the user
            * "Revert scanner options to default values": resets values in the options sidebar

Multi-page/PDF is not supported (Skanpage in development as prospective successor).

Takeaways:

* Why have a pop-up window for regular "Scan"? Why not use the existing page view, instead of leaving the low-quality preview scan there? The only additional action in the pop-up is "Save", which shouldn't necessitate opening a new window.
    * "Preview before saving" is just as bad because now I don't get to see at all what I scanned.
* "Preview" plus area size selection plus "Scan" is worse than "Scan" plus "Crop" in most cases.
    * I usually know how large my paper size is (A4, Legal), ideally I'd select that and not have to deal with millimeters or selections (unless going for that specifically).
* Inherits Kooka's still-annoying device selection dialog at startup, but at least it disappears automatically when only a single device is recognized.
    * Usually there are two devices: scanner and laptop camera. It should pre-select the scanner and not require an additional confirmation before starting the scanning.
* "Help", "About" and "Close" buttons should not get such prominent placement and use up space.
* Having both a side bar and a bottom bar (especially with two rows) reduces the page view size by a lot. It's also bad for mobile form factors. Try to stick to a single edge if possible.
* "Revert scanner options to default values" might be better off adjacent to the actual options that it resets.
* Is "Invert colors" really a basic option? Just because it's something that can be done independently from scanner features doesn't necessarily mean that people need it much.
* Is it possible to do without the confusion of white level added to brightness and contrast? Most people don't even know what the difference between brightness and white level is, in addition to brightness/contrast being a derivative of white & black levels to begin with.
* Selections are confusing - on preview scan it frames a part in red, but that's not active until I click the minus button at the center of the pre-selected part. Until then, it still scans the whole page.
    * Why is it a minus button if I add a selection, and plus when I remove it again?
    * Why does the selection expand (or change in other ways) when I click the minus button?
    * Why is there a concept of multiple selections (red and white) when I can only use one anyway?
    * Why can't I select and crop after having done the actual scan instead?
* Having a UI for scanner specific options is useful.
* Auto-sizing is really useful if you don't need to do pixel-perfect editing.


Perspective-correcting page capture from camera
-----------------------------------------------

Many people don't use flatbed scanners anymore, they use their phone camera instead. KDE should provide useful functionality as phone OS, too. It might make sense to expand the scope of the app to capturing an image, detecting a sheet of paper in it (might not be 100% rectangular) and applying perspective correction plus brightness/contrast adjustment to make it look similar to the original paper.

Not investigating any app right now, but they do exist on Android at least.


Plasma Mobile
-------------

Apps for Plasma Mobile usually have a hamburger menu on the bottom left (in LTR order), often a "main" emphasized action button in the bottom center (optionally surrounded by one or two related actions such as up/down or discard/confirm) and perhaps another action button in the bottom right. Some apps use different layouts though, like the calculator with a tab bar for calculator layouts, or the browser with an address bar (but also including the hamburger menu on the bottom left).

Hamburger menus in a Plasma Mobile app open a slide-in sidebar called Global Drawer. There is also an optional bottom-right action button counterpart called Context Drawer, which displays further actions for currently selected items.

The top edge is often, but not necessarily, occupied by a title for the current view or section.
