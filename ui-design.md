Flow and UI design
==================

Purpose
-------

Marascan's primary goal is to help the user with capturing sheets of paper (documents, photos) as PDF or image files. This means the app has two main responsibilities:

* image acquisition via scanner or camera, as well as
* post-processing such as filters, positioning or ordering pages.

Marascan follows the KDE philosophy of ["simple by default, powerful when needed"](https://develop.kde.org/hig/). It should be straightforward to start scanning with minimal setup, intuitive to tweak image quality settings and get a better take, and easy to add more pages for a multi-page document. The user shouldn't have to know about each possible option, but advanced configuration and editing functionality is not out of place assuming it doesn't impede basic use of the app.

Target form factors are desktop and mobile (phone, tablet, portable gaming PC) devices. This means we need to consider UI flows for horizontal and vertical app layouts. It also means we need to think about what parts of the UI to display given different window sizes.


Comparative analysis
--------------------

[comparative-analysis.md](comparative-analysis.md) looks at a few other scanning & PDF organization apps to get a sense of possible workflows, differences, strengths and weaknesses. This informs the feature set outlined here as "requirements".


Core features
-------------

Actions that every user has to take to make it through the simplest possible workflow:

* Look at the default-initialized scanning device and nod, or change the image source.
* Press "Scan" to acquire the image/page
    * Ideally the size is automatically picked based on scanned image and/or locale
* See the scanned image/page on screen
* Press "Save" to open a Save File dialog and save it to disk (as PDF by default)

Other important features are not always crucial, but used regularly:

* Delete Page, if the scan didn't turn out well & needs to be redone
* Discard, to restart from scratch
    * In practice, this is mostly "Delete" for all pages, but also any PDF metadata and disassociation from any file path (for saving)
    * Could be called New Document, but that's ambiguous whether the current document is replaced or a new window gets opened
    * Could be called Clear like in Skanpage, but Discard better expresses the "completely start from scratch" aspect as opposed to just "delete all pages"
    * Theoretically, "Save" could always discard the current document and further reduce the need for this (but perhaps that's unintuitive)
* Rotate (left/right)
* Crop
* Zooming, for toggling between document organization and page inspection
    * Newly scanned pages are most in need of inspection, previous ones are probably fine
* Preset, at the beginning of a page batch
    * Different settings are rarely necessary within the same document
    * What's part of a preset? I feel like scan quality / color settings should be, whereas page format & feeder mode settings change more frequently and should be independent
* Page format
* Single-page scan or feeder
    * Not always relevant for casual users without feeder
    * But people with office scanners who regularly switch will need this badly
    * Feeders can usually scan front, back or both sides, in addition to continuing after the first page or stopping
* Import / Open Document
    * Only "important" if we want to emphasize the PDF organization functionality
* Reorder Pages, if a page was missed and now has to be inserted into the middle
* Display of selected page numbers and total page count, for keeping track of larger documents

Several options are also very important, but can hopefully be set once and mostly reused thereafter (probably as part of a preset):

* Page format, because automatic size selection will always be fragile
* DPI
* Gray/Color, for different document types and user preferences
* Brightness/Contrast, due to scanner defaults and user preferences

The rest can probably be tucked away in a menu or expanding view at all times.


Context dependency
------------------

We can't have a button for all of the aforementioned important features at the same time, but perhaps we can have a button for all of them at the right time, without showing them all at once. Let's see in which situations an action or option is irrelevant or can otherwise be hidden.

* Hamburger menu / Global Drawer
    * Always required
* Image source & scan options (preset, page format, DPI, grayscale/color, brightness/contrast, feeder, etc.)
    * Usually only necessary to get the first page right
    * Subsequent pages can mostly reuse the same settings
    * Can disappear once pages are shown in the page view
    * Should be possible to bring back via button or menu item though
    * Options that are part of a preset could also go into an "Edit preset" dialog/view
* Scan
    * Not functional unless image source has been selected (and options, but we can always have some default options at least)
* Save
    * Not functional unless pages have been scanned or imported
* Discard
    * Not functional unless pages have been scanned or imported
* Import / Open Document
    * Mostly useful for bootstrapping, i.e. before pages have been scanned or imported
    * Has a secondary use for combining documents, i.e. should exist as a menu item either way
* Rotate left/right
    * Only functional if one or more pages are selected
    * Could be done as (selected) page overlay to save toolbar space
        * Pages aren't usually square, there's often some available corner space if we put a page into a slightly larger frame (which is useful anyway for showing page selection)
* Delete Page
    * Only functional if one or more pages are selected
        * Also a candidate for page overlays
    * But could double up as Discard if no pages are selected (after confirmation dialog)
        * If we do this, it should rather get a (semi-)permanent action button
* Crop
    * Only functional if one or more pages exists
    * Page-specific like Delete Page and Rotate, but starts a mode instead of performing an immediate action
    * Page selection could also happen by starting a rectangle drag on any visible page
    * Nothing wrong with a (selected) page overlay though
* Page numbers
    * Only useful if more than one page has been scanned or imported
    * Also a candidate for page overlays
* Zoom
    * On mobile, pinch zoom is common and expected - no button(s) required
    * On desktop, Ctrl + mouse wheel is common, but buttons or zoom slider is also useful
        * Button/slider behavior depends on how Multi-Page and Full Page views resize & reflow
        * If using auto-resize based on window size, no actions are necessary
        * It's thinkable to allow zoom for either Multi-Page or Full Page view, but not both
* Reorder Pages
    * Most common use cases can be done via drag-and-drop, no button(s) required
    * More complicated reordering (reverse, interleave) can go into a menu


A note on Import, Open Document and Save
----------------------------------------

The action named Import or Open Document does more or less the same thing regardless of its name: load pages from an existing (image or PDF) file and insert them into the current document. Usually used when no other pages exist, as starting point for PDF organization or adding more pages to an existing PDF.

The naming of the action makes for a subtle difference:

* Open Document implies that after loading, the opened document still represents the same file. There will be an "opened file path" variable associated to the document and the Save action will by default overwrite the same file, possibly even without further confirmation (but not necessarily so).
* Import implies that the loaded pages are always a copy of the original file, no "opened file path" is associated to the current document when importing pages. The Save action will always default to saving to a new file. (That said, perhaps it can open the "Save File" dialog in the same directory as the imported file.)

The difference is subtle, minimal and confusing enough that there should really only be one of these two options in the app. PDF Arranger uses the hover text "Open a file and append it to the current document" for its prominent toolbar action, which has mostly Import semantics but stores the filename for its Save function that behaves like Save As on first use. It also keeps separate Save and Save As buttons in its toolbar anyway. I like this flow - not 100% sure about their naming, but we may well consider using this and just sticking with Import (or Import Document) for consistency.

We'll also want to relegate Save As to a menu item, it doesn't seem important enough for a space-limited toolbar.


Convergent UI proposal
----------------------

Based on the above, it seems like we should be able to cover a lot of use cases with a manageable amount of permanently visible actions. Here's a proposal for UI design based on KDE HIG guidelines, usable on both desktop and mobile devices.

**Page view**

* Covers the whole window, except the bottom action buttons
* Mobile: Never show Page view without pages existing, switch to Options view instead
* Desktop: Disable page-specific actions unless at least one page exists
* No horizontal scrollbars ever, all scrolling is vertical
* Pages always get framed in a slightly bigger rectangle, highlighted borders if selected and transparent if not
* Mouse: Clicking a page selects it, once more (not double-click) deselects, clicking another page moves the selection
    * Ctrl-Click selects multiple pages, Shift-Click selects ranges (like usual)
* Touchscreen: Tapping a page selects, once more (not double-tap) deselects, tapping another page moves the selection
    * Long-press + vibrate + release without (much of a) move enters multi-page selection mode, where a regular tap adds more pages to the selection
        * (jpetso: I only know this from Android where it also gets a highlighted top bar with Back button, can we have that for mobile KDE too?)
* When one or more pages are selected, a hovering mini-toolbar should appear with selection quick action icons: Rotate Left, Rotate Right, Crop
    * Ideally in vertical alignment and with Scrim background?
    * Fades in next to the last-selected page
* Drag-and-drop of a page selection allows reordering pages
    * Drag-and-drop on Desktop: mousedown + move (you know it)
    * Drag-and-drop on Mobile: long-press + vibrate + move
    * Draw insertion preview marker between pages to indicate where they get moved to
        * Possibly better (but luxury): Move actual page graphics around while dragging
            * Possibly hard to get right, I'd stick to insertion preview markers for now
    * If multiple non-contiguous ranges get dragged, they will move relative to each other, ideally with several insertion preview markers before dropping
* Full Page view mode
    * When exactly one page exists, Full Page view mode is automatically entered and cannot be switched to Multi-Page view mode
    * When more than one page exists, double-tap switches to Multi-Page view mode with that page visible in the view and selected
        * Touchscreen only: Pinch zoom out (farther than a single row xor column possible) switches to Multi-Page view mode
    * Full Page view mode is auto-resize: pages get sized to fit view width & height
        * Pages will never exceed view size and get vertically centered within the view
        * If two or more pages fit into one view (vertically XOR horizontally), the two or more get displayed at the same time
        * Continuous vertical scrolling *could* be worthwhile, but will be difficult to get right together with auto-resizing by window/view size
        * As a workable approach, allow scrolling but snap to page (center on mouse-wheel or flick release) and don't auto-resize until scrolling is done
        * Continuous scrolling, if later implemented correctly, could be added as a setting
* Multi-Page view mode
    * Double-click/-tap on a page switches to Full Page view mode with that page centered and selected
    * Pages are zoomed rather small, aligned in a grid
        * Linebreaks for the grid at the right window edge, scrolls vertically
    * Mouse: Ctrl-Mousewheel zooms in or out, reflow at every step
    * Touchscreen: Pinch zoom zooms in or out, reflow pages when pinch is released
        * If user pinches too far into a page, switch to Full Page view mode for the centered page instead and deselect pages out of view

**Options view**

* Mobile: Temporarily replaces the Page view when options are being selected/edited
* Desktop: Temporarily slides in from the left when options are being selected/edited (shifting Page view to the right but not disabling its functionality), and can also be pinned to avoid automatically sliding back
    * Options view probably doesn't work great in a Page Stack with the Page view, because it can spawn Edit Preset views in between and on mobile, only the deepest stack element is shown. Page view is not a child of Edit Preset semantically, and would require different treatment on Desktop vs. Mobile if it works at all.
    * Stacking Options as a child of the Page view (instead of Options preceding Page view in the stack) could make sense semantically, but then it would show up on the right window edge on desktop. Getting this to the left as optional (hidden if narrow) view seems fraught with danger.
    * So one probably needs a custom construct to make this work well. Likely have Options as a (pinnable) independent page stack and Page view just being a standalone view, with some logic to communicate.
* Image Source drop-down
    * Determines what remaining options are shown
    * Have a little refresh button to the right of the drop-down to reload sources
    * Combine input device + SANE's "Scan source" (Flatbed, Feeder, see Skanlite) into a single list, e.g.
        * Noname Integrated Camera: Integrated C
        * Canon PIXMA MX340 - Flatbed
        * Canon PIXMA MX340 - Auto Document Feeder
* Page Format drop-down with editable preset buttons (see below)
    * By default, contains most of the usual paper formats
        * From simple-scan: Automatic, A6, A5, A4, A3, Letter, Legal, 4+6
    * Copy + Confirm buttons may add extra formats
* Quality (i.e. preset) drop-down with editable preset buttons (see below)
    * By default, contains the following options:
        * "Text (color)" (color, 300 dpi) (default)
        * "Text (grayscale)" (grayscale, 300 dpi)
        * "Image" (color, 600 dpi)
    * Also includes Brightness, Bit Depth, Contrast, Invert Colors
    * Also includes scanner-specific options unless listed under the top-level explicitly
    * When not editing, the selected options are shown below the drop-down row as text
        * Try not to display unchanged defaults other than the "big two" (color mode, DPI)
* Feeder options, if supported by the scan source
    * Front, Back, Both (drop-down)
    * Single Page, All Pages from Feeder (drop-down)
        * simple-scan also has Multiple Pages from Flatbed, but what does it actually do? Is it just a loop for the same device? With scan delay from Preferences?
* Remainder of scanner-specific options if not already covered above

**Action buttons (mobile: bottom, desktop: top)**

* Leftmost: Hamburger menu (Global Drawer) - see Global Drawer contents below
* Primary Action - Page view (normal):
    * Main: Scan
        * Turns into Stop/Cancel until page is fully scanned
    * Left (one or more pages selected): Delete
    * Left (no selection): Discard
        * Asks for confirmation if Page view holds two or more pages
        * This could realistically be Delete like with a selection (functionality would be similar except for clearing metadata) but the real reason for the switch is to signal to the user that Delete works just on selected pages and won't delete everything.
    * Right: Save
        * Disabled while a page is being scanned
* Primary Action - Page view (Crop Rectangle mode):
    * Main: Crop (confirms the current rectangle and crops page)
    * Left: Cancel
    * Right: Open "Crop by Measurements" dialog - see Notes on Crop below
* Primary Action - Image Source selection:
    * Main: Refresh
    * Left (if pages exist, but Page view not visible because Mobile): Confirm, return to Page view
    * Right: Import / Open Document
* Primary Action - Options view:
    * Main: Scan (only on Mobile, because Page view is always visible on Desktop)
    * Left (if pages exist, but Page view not visible because Mobile): Confirm, return to Page view
    * Right: Import / Open Document
* Primary Action - Edit Preset view (child of Options view):
    * Main: Scan (only on Mobile, because Page view is always visible on Desktop)
        * Inherited from Options
        * Mobile: Save and close (i.e. Confirm) before Scan
        * Desktop: Save before Scan, but don't remove from Page Stack - user can explicitly press Confirm for going back to Options, but can also continue editing instead
    * Left: Cancel
        * Closes the Edit Preset page without saving, back to Options view
    * Right: Confirm
        * Saves and closes the Edit Preset page, back to Options view
        * Confirmation dialog ("overwrite?") if choosing a different existing custom preset name
* Rightmost: Context Drawer
    * For Page view (all or selected pages) - see Context Drawer contents below
    * For Edit Preset view - see "Drop-down with editable preset buttons" layout & behavior

Alternative idea: Use the "Primary Action (left)" button as switch between Full Page and Multi-Page view modes, leaving Delete merely as context/overlay action and Discard to the Global Drawer menu. This has the appeal of "Primary Action (left)" always being responsible for view switching. However, I don't like it quite as much because it allows us to switch from Multi-Page to Full Page view mode without touching the Page view with mouse or fingers, so I'd expect to keep my selection intact but I don't see it anymore? That could end up with confusing selection behavior.

---

**Global Drawer contents** (hamburger menu slide-in):

* Status header: document name / filename, `$N pages`
* "File export" icon row (all disabled outside of Page view):
    * Save
    * Share
    * Print
* Discard
* Import
* Insert Blank Page (append to end, can always be reordered later)
* Scan Options (perhaps temporarily rename to Photo Options if only a camera is found?)
    * omit if Scan Options is always present in the Page Stack
* Settings
* Help
* About

---

**Context Drawer contents** (overflow menu slide-in) - acts on selected pages only, or if none selected, on all pages:

* Status header: `Selected pages: $N[-$M][,$etc] of $total`, or `1 page` / `All $N pages`
* "Standard Edit actions" icon row:
    * Cut
    * Copy
    * Paste - see "Notes on Inserting Pages" below
* Selection >
    * Select All
    * Select Odd Pages
    * Select Even Pages
    * All From Same File (keep track of file origins)
    * Same Page Format
    * Invert Selection
    * Clear Selection
* Export Pages
    * Opens modal dialog with Export Selection to a Single File, Export Selection to Individual Files
* Reorder Pages
    * Opens modal dialog with Reverse Pages, Combine Pages, Combine Pages (Reverse) options and Cancel
        * Dialog should also mention that reordering pages is possible with drag-and-drop
* OCR (possible future feature - specification/design TODO)
* Rotate Left
* Rotate Right
* Scale (possible future feature - specification/design TODO)
* Crop - see Notes on Crop below
* "Standard Editing footer" icon row:
    * Delete
    * Undo
    * Redo

If we implement all of these features, we might consider to throw out Rotate Left/Right and Crop and put them just in the pop-up floating toolbar that appears when selecting pages. However, that would remove the option to rotate all pages when none are selected (and require the user to first Select All). I think a better option is to collapse Rotate Left and Rotate Right into a single menu entry (with child actions) and still get quick access via floating toolbar, so going through the nested menu doesn't hurt so much.

---

**"Drop-down with editable preset buttons"** layout & behavior:

* To the right of the drop-down list, an Edit icon button
* Edit pushes an Edit Preset view onto the Page Stack
    * Name field with preset name, changing it will modify the custom preset
    * Form fields for adjusting the preset (differs)
* If the preset is changeable (and/or already custom)
    * Use verbatim $name for Name field
* If the preset is marked as unchangeable hardcoded preset (e.g. standardized page formats)
    * Clone it as "$name (clone)" and focus the Name field on first appearance for keyboard edit
        * Easier to edit than "Copy of $name", especially on mobile
        * Also slightly more compact
    * Add a message that $name cannot be changed and its settings were cloned instead
* Primary Actions: see Action buttons section (Scan, Confirm, Cancel)
* Context Drawer:
    * Clone - should be disabled if no changes were made to original preset options
    * Delete Preset - remove the custom preset from the drop-down list, select a different item instead

---

**Notes on Inserting Pages**:

* Apart from Scan itself, there are three sources for inserting pages:
    * Import / Open Document
    * Insert Blank Page
    * Paste
        * Unlike the other two, Paste isn't meant be used to initialize a document, so it sits in the Context Drawer next to Cut & Copy
* There are several ways that pages can be inserted:
    * Insertion start
        * At the end of the document
        * At the start of the document
        * Before the first selected page
        * After the last selected page
    * Interleaving
        * Insert first page at insertion start and interleave from there
        * Insert first page at insertion start + 1 and interleave from there (weird for "After the last selected page" start)
            * simple-scan has Interleaved Odd and Interleaved Even, is that the same difference?
        * After determining insertion start, ignore selection to avoid weirdness with premature selection end or non-adjacent ranges
        * Interleave only with selected pages, deal with premature selection end and non-adjacent ranges somewhat intuitively (is there such a thing?)
    * Insertion order
        * In order
        * Reverse order
* In order to remain somewhat simple but still offer these options to the user, we:
    * Behave the same way for Import, Insert Blank Page and Paste
    * Always insert at the end of the document if no pages are selected
        * No extra dialog, perform the action immediately
        * This should cover the majority of use cases without confusing users
    * Select all inserted pages after insertion, discarding any prior selection
    * Always insert in order - reversing can be easily done post-hoc with Reorder Pages
    * Leave out an "interleave starting from insertion start + 1" option - user can drag and drop if insertion start didn't work out for them
    * Pop up a modal dialog with advanced paste options if a page selection exists
        * Title: "Import $n pages" / "Insert Blank Page" / "Paste $n pages" (depending)
        * Toggles: "Adjacent" / "Interleaved" (ideally with illustration)
        * Action buttons (performing insertion, closing dialog):
            * Insert Before Selection
            * Insert After Selection
            * Append at End of Document

---

**Notes on Crop**:

* There are three ways of cropping pages:
    * By manually drawing a rectangle and confirming it
    * By automatically detecting page boundaries and cropping to that rectangle
        * Variant #1: Lowest common boundary offsets for all selected pages
        * Variant #2: Individual (tighter) boundary offsets for all selected pages
    * By specifying page coordinates in units or percentages from current page boundary
* If a single page is selected, enters Crop Rectangle mode targeting that page
    * Darkened area outside of corners and present crop rectangle à la Gwenview or simple-scan
    * Scan action & Co. turn into crop confirm/cancel/dialog actions
    * Consider auto-switching to Full Page view mode?
    * Primary Actions for Crop Rectangle mode include "Crop by Measurements"
* If multiple pages are selected, go straight to "Crop by Measurements" dialog
* Crop by Measurements dialog contents
    * If multiple pages are selected, provide a note that drawing a crop rectangle manually is only possible when a single page is selected
    * Tab bar for measurement types:
        * Automatic
            * Crop Rectangle Detection (drop-down):
                * Use detected size for each page individually
                * Use largest detected size for all pages
            * Detect White Borders (button): exits dialog, shows a preview in Crop Rectangle mode (if necessary, switch to Multi-Page view mode)
        * Units
            * Unit of Measurement: Percent, Millimeters, Centimeters, Inches, etc.
                * Perhaps we can grab this from platform preferences?
            * Offsets from (in the selected unit):
                * Left
                * Right
                * Top
                * Bottom
            * Set Crop Selection (button): exits dialog, shows a preview in Crop Rectangle mode (if necessary, switch to Multi-Page view mode)
        * If dialog was canceled without setting a crop rectangle, and no rectangle existed before (coming directly from multi-page selection Crop action), exit Crop Rectangle mode before it even really started
        * Press Crop (main Primary Action) to confirm rectangle selection, or go back into the dialog with right Primary Action
